//app.js
App({
  //当小程序启动，或从后台进入前台显示，会触发onShow事件
  onShow: function () {
    var that = this;
    if (that.globalData.userInfo && that.globalData.currCity) return;
    that.getSessionID();
    //获取位置信息
    wx.getLocation({
      type: 'wgs84', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
      success: function (res) {
        // success
        var lat = res.latitude,
          lng = res.longitude;
        //通过腾讯地图API接口根据经纬度获取位置信息
        wx.request({
          url: 'https://apis.map.qq.com/ws/geocoder/v1/',
          data: {
            location: lat + ',' + lng,
            key: "X2VBZ-EZA62-5J4UA-CYZUE-3N6YT-5ABFM"
          },
          method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
          success: function (res) {
            // success
            const { city } = res.data.result.address_component;
            that.globalData.currCity = city;
          },
          fail: function () {
            // fail
            console.log('获取位置信息失败。');
          }
        })
      },
      fail: function () {
        // fail
        console.log('获取经纬度信息失败。');
      }
    });
  },
  getSessionID: function (callback) {
    const that = this;
    //调用登录接口获取session ID
    wx.login({
      success: function (response) {
        //发送请求到自己的服务器
        if (response.code) {
          //获取用户信息
          that.getUserInfo((user) => {
            wx.request({
              url: 'https://wxapi.dobit.top/Api/User',
              data: {
                code: response.code,
                avatarUrl: user.avatarUrl,
                nickName: user.nickName
              },
              method: 'GET',
              success: function (res) {
                // success
                const { State, Message, Result } = res.data;
                if (State === 1) {
                  wx.setStorageSync('session_id', Result)
                  callback && callback(sessionID);
                } else {
                  console.log('获取session ID失败');
                }
              },
              fail: function () {
                console.log('fail');
              }
            });
          });
        }
      }
    });
  },
  getUserInfo: function (cb) {
    var that = this
    if (this.globalData.userInfo) {
      typeof cb == "function" && cb(this.globalData.userInfo)
    } else {
      //获取用户信息
      wx.getUserInfo({
        success: function (res) {
          that.globalData.userInfo = res.userInfo
          typeof cb == "function" && cb(res.userInfo)
        }
      });
    }
  },
  globalData: {
    userInfo: null,
    currCity: ''
  }
})