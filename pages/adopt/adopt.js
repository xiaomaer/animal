var app = getApp();
Page({
  data: {
    page_num: 1,
    page_total: 1,
    page_size: 6,
    query_str: "",
    list: [],
    isShow: false,
    reminder: "",
    tipText: '',
    showTips: false,
    isRefresh: false,
    placeholder: '没喜欢的吗？搜一下'
  },
  onLoad: function () {
    var that = this;
    //显示目前所在城市
    that.setData({
      showTips: true,
      tipText: "当前所在城市：" + app.globalData.currCity
    });
    //隐藏显示目前所在城市
    setTimeout(function () {
      that.setData({
        showTips: false
      });
    }, 1500);
    that.getAnimalList(true, false);
  },
  onPullDownRefresh: function () {
    // 下拉刷新
    this.setData({
      isShow: false,
      page_num: 1,
      page_total: 1,
      isRefresh: true
    });
    this.getAnimalList(true, true);
  },
  onReachBottom: function () {
    // 上拉加载更多
    if (this.data.isRefresh) {
      //下拉刷新触发该事件时
      this.setData({
        isRefresh: false
      });
      return;
    } else {
      this.setData({
        page_num: this.data.page_num + 1
      })
      this.getAnimalList(false, false);
    }
  },
  onShareAppMessage: function () {
    //页面分享
    return {
      title: '领养萌宠',
      desc: '领养代替购买，爱心的你把萌萌的它带回家吧！',
      path: '/adopt/adopt'
    }
  },
  changeQuery: function (e) {
    this.setData({
      query_str: e.detail.value,
      page_num: 1,
      page_total: 1
    });
  },
  search: function (e) {
    this.getAnimalList(true, false);
  },
  getAnimalList: function (isFirst, isRefresh) {
    const that = this,
      { page_size, page_num, page_total, query_str } = that.data;
    if (!isFirst && (page_num > page_total)) {
      that.setData({
        reminder: "已显示全部内容",
        isShow: true
      });
      return;
    }
    //解决下拉刷新页面卡住的问题
    // wx.showToast({
    //   title: '加载中',
    //   icon: 'loading'
    // });
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Info',
      data: {
        'Type': 0,
        'IsOwn': false,
        'City': app.globalData.currCity,
        'Query': query_str,
        'Page': page_num,
        'Size': page_size
      },
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        // wx.hideToast();
        //停止下拉刷新
        if (isRefresh) {
          wx.stopPullDownRefresh();
          that.setData({
            isRefresh: false
          });
        }
        if (res.statusCode === 200 && res.data.State === 1) {
          var data = res.data.Rows;
          if (data.length === 0) {
            that.setData({
              list: [],
              reminder: "没有符合条件的信息",
              isShow: true
            });
            return;
          } else {
            that.setData({
              isShow: false
            })
          }
          for (let i = 0, len = data.length; i < len; i++) {
            var createTime = data[i].CreateDateTime.substr(0, 10);
            data[i].CreateDateTime = createTime;
          }
          //onload或refresh时计算总页数
          if (isFirst) {
            that.setData({
              list: data,
              page_total: Math.ceil(res.data.Total / page_size)
            })
          } else {
            that.setData({
              list: that.data.list.concat(data)
            });
          }
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '请求失败',
            content: '刷新页面试试',
            showCancel: false
          })
        }
      },
      fail: function () {
        // fail
        console.log('获取领养宠物列表失败');
      }
    });
  }
})