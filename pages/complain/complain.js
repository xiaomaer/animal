const qiniuUploader = require("../../utils/qiniuUploader-min");
Page({
  data: {
    imageList: [],
    imageUrls: [],
    unUploadImg: [],
    isFinish: true,
    tipText: '',
    showTips: false,
    userID: '',
    articleID: ''
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.setData({
      userID: options.userID,
      articleID: options.articleID
    })
  },
  showTips: function (tipText) {
    var that = this;
    that.setData({
      showTips: true,
      tipText: tipText
    });
    setTimeout(function () {
      that.setData({
        showTips: false
      });
    }, 1000);
  },
  previewImage: function (e) {
    //预览图片
    var current = e.target.dataset.src
    wx.previewImage({
      current: current,
      urls: this.data.imageList
    });
  },
  chooseImage: function (e) {
    //选择和上传图片
    var that = this,
      tempList = that.data.imageList,
      len = tempList.length;
    if (len > 9) {
      wx.showToast({
        title: "最多上传5张图片"
      });
      setTimeout(function () {
        wx.hideToast();
      }, 1000);
      return;
    }
    //选择图片
    wx.chooseImage({
      count: 9, // 最多可以选择的图片张数，默认9
      sizeType: ['original', 'compressed'], // original 原图，compressed 压缩图，默认二者都有
      sourceType: ['album', 'camera'], // album 从相册选图，camera 使用相机，默认二者都有
      success: function (res) {
        // success
        var resFile = res.tempFilePaths;
        that.setData({
          imageList: tempList.concat(resFile),
          isFinish: false,
          unUploadImg: that.data.unUploadImg.concat(resFile)
        });
        //使用七牛上传图片并把返回的图片url存储于imageUrl
        that.uploadImgs();
      }
    });
  },
  uploadImgs: function () {
    var that = this,
      tempUrl = that.data.imageUrls,
      curImage = that.data.unUploadImg.shift();
    if (!curImage) {
      that.showTips('图片上传完成');
      return;
    }
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Qiniu',
      data: {},
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        // success
        var token = res.data;
        qiniuUploader.upload(curImage, (res) => {
          //上传成功
          that.setData({
            imageUrls: tempUrl.concat(res.imageURL)
          });
          that.uploadImgs();
        }, (error) => {
          //上传失败
          console.log('error' + error);
          app.getSessionID();
        }, {
            uploadURL: 'https://up-z2.qbox.me',
            uptoken: token
          });
      },
      fail: function () {
        // fail
        console.log('fail');
      },
      complete: function () {
        // complete
        console.log('complete');
      }
    })

  },
  handleSubmit: function (e) {
    const that = this,
      { detail, tel } = e.detail.value;
    if (detail === "") {
      that.showTips('请输入举报理由');
      return;
    }
    if (that.data.unUploadImg.length !== 0) {
      that.showTips('图片未上传完成');
      return;
    }
    if (tel === "") {
      that.showTips('请输入手机号码');
      return;
    }
    if (!that.checkPhone(tel)) {
      that.showTips('手机号码格式不正确');
      return;
    }
    const { imageUrls, articleID, userID } = that.data;
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Report',
      data: {
        ImageUrls: imageUrls,
        InformationID: articleID,
        Content: detail,
        Phone: tel,
        CreateUserID: userID
      },
      method: 'POST',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        // success
        if (res.statusCode === 200 && res.data.State === 1) {
          wx.showModal({
            title: '举报成功！',
            content: '您的举报已收到，我们将会尽快处理哦',
            success: function (res) {
              if (res.confirm) {
                //返回上个页面
                wx.navigateBack();
              }
            }
          })
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '提交举报失败!',
            content: '请重新发布试试',
            showCancel: false
          })
        }
      }
    })
  },
  checkPhone: function (phone) {
    return /^1[34578]\d{9}$/.test(phone);
  }
})