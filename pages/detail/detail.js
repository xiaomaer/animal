var app = getApp();
Page({
  data: {
    type: "",//领养还是寻找
    id: 0,//文章ID
    // list: [],
    detail: {},
    comments: [],
    total: 0,
    focus: false,
    userID: 0,
    placeHolder: '写留言'
  },
  onLoad: function (options) {
    // 生命周期函数--监听页面加载
    this.setData({
      ...options
    })
    //options.id为文章ID
    this.getAnimalInfo(options.id);
    //领养获取推荐列表
    // if (options.type === '0') {
    //   this.getRecommendList();
    // }
    //获取评论列表
    this.getCommentList(options.id);
  },
  onShareAppMessage: function () {
    return {
      title: '宠物详细信息哦',
      desc: '有爱的你你，这里可以看到宠物的详细介绍哦，点击进入查看哦哦。',
      path: 'detail/detail'
    }
  },
  //获取宠物详情
  getAnimalInfo: function (ID) {
    var that = this;
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    });
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Info/' + ID,
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        wx.hideToast();
        // success
        if (res.statusCode === 200) {
          var data = res.data;
          var createTime = data.CreateDateTime.replace('T', ' ');
          data.CreateDateTime = createTime;
          that.setData({
            detail: data
          });
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '获取宠物详情失败',
            content: '刷新试试',
            showCancel: false
          })
        }
      },
      fail: function () {
        // fail
        console.log('获取宠物详情失败');
      }
    })
  },
  //获取推荐宠物列表
  /*  getRecommendList: function () {
     var that = this;
     wx.request({
       url: 'https://wxapi.dobit.top/Api/Info',
       data: {
         'Type': 0,
         'IsOwn': false,
         'Query': '',
         'Page': 1,
         'Size': 6,
         'City': app.globalData.currCity
       },
       method: 'GET',
       header: {
         SessionID: wx.getStorageSync('session_id')
       },
       success: function (res) {
         // success
         if (res.statusCode === 200 && res.data.State === 1) {
           var data = res.data.Rows;
           for (let i = 0, len = data.length; i < len; i++) {
             var createTime = data[i].CreateDateTime.substr(0, 10);
             data[i].CreateDateTime = createTime;
           }
           that.setData({
             list: data
           });
         } else {
           wx.showModal({
             title: '获取推荐宠物列表失败',
             content: res.data.Message,
             showCancel: false
           })
           app.getSessionID();
         }
       }
     });
   }, */
  //获取评论信息，input并获取焦点
  getCommentInfo(e) {
    const currTarget = e.currentTarget;
    const { userID, userName } = currTarget.dataset;
    this.setData({
      focus: true,
      userID: parseInt(userID),
      placeHolder: userName
    })
  },
  //输入框，失去焦点时，重置信息数据
  resetInfo() {
    this.setData({
      focus: false,
      userID: 0,
      placeHolder: '写留言'
    })
  },
  //获取文章评论列表
  getCommentList(ID, size) {
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Comment/' + ID,
      data: {
        Page: 1,
        Size: size || 10
      },
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: (res) => {
        if (res.statusCode === 200 && res.data.State === 1) {
          const { Total, Rows } = res.data;
          this.setData({
            comments: Rows,
            total: Total
          })
        } else {
          wx.showModal({
            title: '获取评论列表失败',
            content: res.data.Message,
            showCancel: false
          })
          app.getSessionID();
        }
      }
    })
  },
  publish_comment: function (e) {
    const { comment } = e.detail.value;
    const { id, userID } = this.data;
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Comment/',
      data: {
        InformationID: id,
        ReplyUserID: userID,
        content: comment
      },
      method: 'POST',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        if (res.statusCode === 200 && res.data.State === 1) {
          wx.showToast({
            title: '留言成功',
            icon: 'success',
            duration: 2000
          })
        } else {
          wx.showModal({
            title: '留言失败',
            content: res.data.Message,
            showCancel: false
          })
          app.getSessionID();
        }
      }
    })
  }
});