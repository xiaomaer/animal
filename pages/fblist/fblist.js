const qiniuUploader = require("../../utils/qiniuUploader-min");
Page({
  data: {
    questions: ['操作问题', '功能异常', '体验问题', '新功能建议', '其他问题'],
    query: '',
    type_index: 0,
    list: [],
    placeholder: '有问题吗？搜一下'
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.getAll();
  },
  toggleMenu: function (e) {
    var old_index = this.data.type_index,
      curr_index = parseInt(e.target.dataset.index) + 1;
    if (old_index === curr_index) {
      this.setData({
        type_index: 0
      })
    } else {
      this.setData({
        type_index: curr_index
      })
    }
  },
  changeQuery: function (e) {
    this.setData({
      query: e.detail.value
    })
  },
  search: function (e) {
    this.getAll();
  },
  //获取符合条件所有类型的反馈列表
  getAll: function () {
    var that = this;
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    });
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Question/List',
      method: 'GET',
      data: {
        Query: that.data.query
      },
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        wx.hideToast();
        // success
        if (res.statusCode === 200 && res.data.State === 1) {
          that.setData({
            list: res.data.Data
          });
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '获取问题列表失败',
            content: '刷新页面试试',
            showCancel: false
          })
        }
      },
      fail: function () {
        wx.showModal({
          title: '获取问题列表失败',
          content: res.data.Message,
          showCancel: false
        })
        wx.hideToast();
      }
    })
  },
  /**
   * 获取指定类型的反馈列表
   * type:1,2,3,4,5
   */
  getFeedbackList: function (type) {
    var that = this;
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    });
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Question',
      data: {
        Type: 1
      },
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        wx.hideToast();
        // success
        if (res.statusCode === 200) {
          var data = res.data.Rows;
          that.setData({
            list: data
          });
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '获取问题列表失败',
            content: '刷新试试',
            showCancel: false
          })
        }
      },
      fail: function () {
        wx.showModal({
          title: '获取问题列表失败',
          content: res.data.Message,
          showCancel: false
        })
        wx.hideToast();
      }
    })
  }
})