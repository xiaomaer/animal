const qiniuUploader = require("../../utils/qiniuUploader-min");
Page({
  data: {
    questions: ['操作问题（操作不顺畅）', '功能异常（不能使用现有功能）', '体验问题（功能找不到）', '新功能建议（一起让它变的更好）', '其他问题（领养问题、寻找问题等）'],
    question_index: 0,
    tips: ['请您选择问题类型帮助我们更快处理您的反馈～', '操作不方便吗？说出您的心声吧，让我们一起把它变的更好哦～', '不能愉快的使用现有功能吗？请您描述问题并上传相关页面截图能方便我们快速解决问题哦～', '用的不爽？快来告诉我们', '快把您想要的新功能说出来吧～', '可输入您在使用中遇到的其他问题哦～'],
    imageList: [],
    imageUrls: [],
    unUploadImg: [],
    isFinish: true,
    tipText: '',
    showTips: false
  },
  showTips: function (tipText) {
    var that = this;
    that.setData({
      showTips: true,
      tipText: tipText
    });
    setTimeout(function () {
      that.setData({
        showTips: false
      });
    }, 1000);
  },
  bindTypeChange: function (e) {
    this.setData({
      question_index: parseInt(e.detail.value)
    });
  },
  previewImage: function (e) {
    //预览图片
    var current = e.target.dataset.src
    wx.previewImage({
      current: current,
      urls: this.data.imageList
    });
  },
  chooseImage: function (e) {
    //选择和上传图片
    var that = this,
      tempList = that.data.imageList,
      len = tempList.length;
    if (len > 9) {
      wx.showToast({
        title: "最多上传5张图片"
      });
      setTimeout(function () {
        wx.hideToast();
      }, 1000);
      return;
    }
    //选择图片
    wx.chooseImage({
      count: 9, // 最多可以选择的图片张数，默认9
      sizeType: ['original', 'compressed'], // original 原图，compressed 压缩图，默认二者都有
      sourceType: ['album', 'camera'], // album 从相册选图，camera 使用相机，默认二者都有
      success: function (res) {
        // success
        var resFile = res.tempFilePaths;
        that.setData({
          imageList: tempList.concat(resFile),
          isFinish: false,
          unUploadImg: that.data.unUploadImg.concat(resFile)
        });
        //使用七牛上传图片并把返回的图片url存储于imageUrl
        that.uploadImgs();
      }
    });
  },
  uploadImgs: function () {
    var that = this,
      tempUrl = that.data.imageUrls,
      curImage = that.data.unUploadImg.shift();
    if (!curImage) {
      that.showTips('图片上传完成');
      return;
    }
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Qiniu',
      data: {},
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        // success
        var token = res.data;
        qiniuUploader.upload(curImage, (res) => {
          //上传成功
          that.setData({
            imageUrls: tempUrl.concat(res.imageURL)
          });
          that.uploadImgs();
        }, (error) => {
          //上传失败
          console.log('error' + error);
          app.getSessionID();
        }, {
            uploadURL: 'https://up-z2.qbox.me',
            uptoken: token
          });
      },
      fail: function () {
        // fail
        console.log('fail');
      },
      complete: function () {
        // complete
        console.log('complete');
      }
    })

  },
  handleSubmit: function (e) {
    const that = this,
      { detail, tel } = e.detail.value,
      { question_index, unUploadImg, imageUrls } = that.data;
    if (question_index == 0) {
      that.showTips('请选择反馈问题类型');
      return;
    }
    if (detail === "") {
      that.showTips('请输入反馈内容');
      return;
    }
    if (unUploadImg.length !== 0) {
      that.showTips('图片未上传完成');
      return;
    }
    if (tel === "") {
      that.showTips('请输入电话号码');
      return;
    }
    if (!that.checkPhone(tel)) {
      that.showTips('手机号码格式不正确');
      return;
    }
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Question',
      data: {
        Type: question_index,
        Content: detail,
        Phone: tel,
        ImageUrls: imageUrls,
      },
      method: 'POST',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        // success
        if (res.statusCode === 200 && res.data.State === 1) {
          wx.showModal({
            title: '提交反馈成功！',
            content: '处理反馈需要0-3天时间，点击“确定”将为您跳转至问题反馈列表。',
            success: function (res) {
              if (res.confirm) {
                wx.redirectTo({
                  url: '../fblist/fblist'
                });
              }
            }
          })
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '提交反馈失败!',
            content: '请您重新发布试试',
            showCancel: false
          })
        }
      }
    })
  },
  checkPhone: function (phone) {
    return /^1[34578]\d{9}$/.test(phone);
  }
})