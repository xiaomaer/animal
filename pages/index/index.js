//index.js
Page({
  data: {
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../seek/seek'
    })
  },
  onLoad: function () {
  },
  onShareAppMessage:function(){
    return{
      title:'萌宠找家',
      desc:'为流浪的萌宠找到温暖的家和爱他的主人哦',
      path:'/index/index'
    }
  }
})
