var app = getApp();
Page({
  data: {
    page_num: 1,
    page_total: 1,
    page_size: 6,
    list: [],
    isShow: false,
    showTips: false,
    type: 0,
    flags: ['已删除', '待审核', '已审核', '已成功', '已拒绝'],
    isRefresh: false
  },
  onLoad: function (options) {
    const that = this;
    const type = parseInt(options.type);//0：领养；1:寻找
    const title = type ? '我发布的寻找' : '我发布的领养';
    wx.setNavigationBarTitle({
      title: title
    })
    that.setData({
      type: type
    })
    that.getAnimalList(true, false);
  },
  onPullDownRefresh: function () {
    // 下拉刷新
    this.setData({
      isShow: false,
      page_num: 1,
      page_total: 1,
      isRefresh: true
    });
    this.getAnimalList(true, true);
  },
  onReachBottom: function () {
    // 拉到底部加载更多
    if (this.data.isRefresh) {
      //下拉刷新触发该事件时
      this.setData({
        isRefresh: false
      });
      return;
    } else {
      this.setData({
        page_num: this.data.page_num + 1
      })
      this.getAnimalList(false, false);
    }
  },
  getAnimalList: function (isFirst, isRefresh) {
    var that = this,
      { page_size, page_num, page_total, type } = that.data;
    if (!isFirst && (page_num > page_total)) {
      that.setData({
        reminder: "已显示全部内容",
        isShow: true
      });
      return;
    }
    // wx.showToast({
    //   title: '加载中',
    //   icon: 'loading'
    // });
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Info',
      data: {
        'Type': type,
        'IsOwn': true,
        'Query': "",
        'Page': page_num,
        'Size': page_size,
        'City': app.globalData.currCity
      },
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        // wx.hideToast();
        //停止下拉刷新
        if (isRefresh) {
          wx.stopPullDownRefresh();
          that.setData({
            isRefresh: false
          });
        }
        // success
        if (res.statusCode === 200 && res.data.State === 1) {
          var data = res.data.Rows;
          if (data.length === 0) {
            that.setData({
              list: [],
              reminder: "您还未发布过相关信息",
              isShow: true
            });
            return;
          }
          for (let i = 0, len = data.length; i < len; i++) {
            var createTime = data[i].CreateDateTime.replace('T', ' ');
            data[i].CreateDateTime = createTime;
          }
          //onload或refresh时计算总页数
          if (isFirst) {
            that.setData({
              list: data,
              page_total: Math.ceil(res.data.Total / page_size)
            })
          } else {
            that.setData({
              list: that.data.list.concat(data),
            });
          }
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '获取列表失败',
            content: '刷新试试',
            showCancel: false
          })
        }
      },
      fail: function () {
        // fail
        console.log('获取寻找宠物列表失败');
      }
    });
  },
  deleteItem: function (e) {
    var that = this;
    wx.showModal({
      title: '确认删除信息吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {
          var id = e.target.dataset.id;
          wx.request({
            url: 'https://wxapi.dobit.top/Api/Status/' + id,
            method: 'DELETE',
            header: {
              SessionID: wx.getStorageSync('session_id')
            },
            success: function (res) {
              // success
              if (res.statusCode === 200 && res.data.State === 1) {
                if (res.data.Result) {
                  wx.showToast({
                    title: '删除成功',
                    icon: 'success',
                    duration: 500
                  });
                  that.setData({
                    query_str: "",
                    isShow: false,
                    page_num: 1,
                    page_total: 1
                  });
                  that.getAnimalList(true, false);
                }
              } else {
                if (res.statusCode === 401) {
                  app.getSessionID();
                }
                wx.showModal({
                  title: '删除失败',
                  content: '重新试试',
                  showCancel: false
                })
              }
            },
            fail: function () {
              // fail
              console.log('删除失败');
            }
          });
        }
      }
    })

  },
  updateItem: function (e) {
    var that = this;
    wx.showModal({
      title: '确认已完成了吗？',
      content: '',
      success: function (res) {
        if (res.confirm) {
          var id = e.target.dataset.id;
          wx.request({
            url: 'https://wxapi.dobit.top/Api/Status/' + id,
            data: {
              'Flag': 3
            },
            method: 'PUT',
            header: {
              SessionID: wx.getStorageSync('session_id')
            },
            success: function (res) {
              // success
              if (res.statusCode === 200 && res.data.State === 1) {
                if (res.data.Result) {
                  wx.showToast({
                    title: '更新状态成功',
                    icon: 'success',
                    duration: 500
                  });
                  that.setData({
                    query_str: "",
                    isShow: false,
                    page_num: 1,
                    page_total: 1
                  });
                  that.getAnimalList(true, false);
                }
              } else {
                if (res.statusCode === 401) {
                  app.getSessionID();
                }
                wx.showModal({
                  title: '更新状态失败',
                  content: '重新试试',
                  showCancel: false
                })
              }
            },
            fail: function () {
              // fail
              console.log('更新状态失败');
            }
          });
        }
      }
    })

  },
  updateInfo: function (e) {
    //修改发布信息，跳转到“发布页面”
    var id = e.target.dataset['id'];
    wx.redirectTo({
      url: '../publish/publish?id=' + id
    });
  }
});