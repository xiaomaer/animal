var app = getApp();
const qiniuUploader = require("../../utils/qiniuUploader-min");
Page({
  data: {
    animals: ['请选择宠物种类...', '猫咪', '狗狗', '兔子', '其他'],
    animal_index: 0,
    imageList: [],
    imageUrls: [],
    unUploadImg: [],
    isFinish: true,
    locationInfo: '',
    type: 0,
    sex: 1,
    tipText: '正在获取位置信息',
    showTips: false,
    detail: {},
    id: -1,
  },
  onLoad: function (options) {
    var that = this,
      id = options.id;
    //修改信息
    if (id) {
      wx.setNavigationBarTitle({
        title: '修改发布消息'
      })
      that.setData({ id: id });
      //获取宠物详情
      that.getAnimalInfo(options.id);
    } else {
      //获取定位信息
      that.setData({
        locationInfo: app.globalData.currCity
      });
    }
  },
  previewImage: function (e) {
    //预览图片
    var current = e.target.dataset.src
    wx.previewImage({
      current: current,
      urls: this.data.imageList
    });
  },
  chooseImage: function (e) {
    //选择和上传图片
    var that = this,
      tempList = that.data.imageList,
      len = tempList.length;
    if (len > 9) {
      wx.showToast({
        title: "最多上传9张图片"
      });
      setTimeout(function () {
        wx.hideToast();
      }, 1000);
      return;
    }
    //选择图片
    wx.chooseImage({
      count: 9, // 最多可以选择的图片张数，默认9
      sizeType: ['original', 'compressed'], // original 原图，compressed 压缩图，默认二者都有
      sourceType: ['album', 'camera'], // album 从相册选图，camera 使用相机，默认二者都有
      success: function (res) {
        // success
        var resFile = res.tempFilePaths;
        that.setData({
          imageList: tempList.concat(resFile),
          isFinish: false,
          unUploadImg: that.data.unUploadImg.concat(resFile)
        });
        //使用七牛上传图片并把返回的图片url存储于imageUrl
        that.uploadImgs();
      }
    });
  },
  uploadImgs: function () {
    var that = this,
      tempUrl = that.data.imageUrls,
      curImage = that.data.unUploadImg.shift();
    if (!curImage) {
      that.showTips('图片上传完成');
      return;
    }
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Qiniu',
      data: {},
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        // success
        var token = res.data;
        qiniuUploader.upload(curImage, (res) => {
          //上传成功
          that.setData({
            imageUrls: tempUrl.concat(res.imageURL)
          });
          that.uploadImgs();
        }, (error) => {
          //上传失败
          console.log('error' + error);
          app.getSessionID();
        }, {
            uploadURL: 'https://up-z2.qbox.me',
            uptoken: token
          });
      },
      fail: function () {
        // fail
      },
      complete: function () {
        // complete
      }
    })
  },
  showTips: function (tipText) {
    var that = this;
    that.setData({
      showTips: true,
      tipText: tipText
    });
    setTimeout(function () {
      that.setData({
        showTips: false
      });
    }, 1000);
  },
  handleType: function (e) {
    //发布类型
    this.setData({
      type: parseInt(e.detail.value)
    });
  },
  handleSex: function (e) {
    //宠物性别
    this.setData({
      sex: parseInt(e.detail.value)
    });
  },
  bindTypeChange: function (e) {
    //宠物种类
    this.setData({
      animal_index: e.detail.value
    });
  },
  selectLoction: function (e) {
    var that = this;
    wx.chooseLocation({
      success: function (res) {
        // success
        that.setData({
          locationInfo: res.address
        })
      }
    })
  },
  handleSubmit: function (e) {
    //发布
    var that = this,
      details = e.detail.value,
      data = that.data;
    //宠物种类、宠物名称、宠物详情、宠物图片、送养原因、领养要求和联系方式不能为空。
    if (details.category == 0) {
      that.showTips('请选择宠物种类');
      return;
    }
    if (details.describe === "") {
      that.showTips('宠物名称不能为空');
      return;
    }
    if (details.info === "") {
      that.showTips('宠物详情不能为空');
      return;
    }
    if (data.unUploadImg.length !== 0) {
      that.showTips('图片未上传完成');
      return;
    }
    if (data.imageUrls.length === 0) {
      that.showTips('请至少上传一张图片');
      return;
    }
    if (details.type == 0) {
      if (details.reason === "") {
        that.showTips('送养原因不能为空');
        return;
      }
      if (details.demand === "") {
        that.showTips('领养要求不能为空');
        return;
      }
    }
    if (details.tel === "" && details.wx === "") {
      that.showTips('电话或微信至少填写一个哦');
      return;
    }
    if (!that.checkPhone(details.tel)) {
      that.showTips('手机号码格式不正确');
      return;
    }
    var api = "https://wxapi.dobit.top/Api/Info",
      method = "POST",
      animal_id = data.id,
      remind = '信息发布成功！';
    if (animal_id !== -1) {
      api = "https://wxapi.dobit.top/Api/Info/" + animal_id;
      method = "PUT";
      remind = '信息修改成功！'
    }
    wx.request({
      url: api,
      data: {
        Age: details.age || '不详',
        Address: data.locationInfo,
        ImageUrls: data.imageUrls,
        Info: details.info,
        Phone: details.tel,
        Reason: details.reason,
        Require: details.demand,
        Sex: details.sex,
        Title: details.describe,
        Category: details.category,
        Type: details.type,
        WeChat: details.wx
      },
      method: method,
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        // success
        if (res.statusCode === 200 && res.data.State === 1) {
          wx.showModal({
            title: remind,
            content: '信息审核需要0-3天时间，点击“确定”将为您跳转至您的发布列表。',
            success: function (res) {
              if (res.confirm) {
                wx.redirectTo({
                  url: '../mine/mine?type=' + details.type
                });
              }
            }
          })
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '信息发布失败!',
            content: '请重新发布试试',
            showCancel: false
          })
        }
      }
    })
  },
  getAnimalInfo: function (ID) {
    var that = this;
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    });
    //获取宠物详情
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Info/' + ID,
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        wx.hideToast();
        // success
        if (res.statusCode === 200) {
          var data = res.data;
          var createTime = data.CreateDateTime.replace('T', ' ');
          data.CreateDateTime = createTime;
          that.setData({
            detail: data,
            locationInfo: data.Address,
            imageUrls: data.ImageUrls || [],
            animal_index: data.Category,
            type: data.Type,
            sex: data.Sex
          });
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '获取宠物详情信息失败',
            content: '刷新试试',
            showCancel: false
          })
        }
      },
      fail: function () {
        // fail
        console.log('获取宠物详情失败');
      }
    })
  },
  checkPhone: function (phone) {
    return /^1[34578]\d{9}$/.test(phone);
  }
})