var app = getApp();
Page({
  data: {
    page_num: 1,
    page_total: 1,
    page_size: 8,
    query_str: "",
    list: [],
    isShow: false,
    tipText: '正在获取位置信息',
    showTips: false,
    isRefresh: false
  },
  onLoad: function () {
    var that = this;
    //获取所在城市
    that.setData({
      showTips: true,
      tipText: "当前所在城市：" + app.globalData.currCity
    });
    setTimeout(function () {
      that.setData({
        showTips: false
      });
    }, 1500);
    that.getAnimalList(true, false);
  },
  onPullDownRefresh: function () {
    // 下拉刷新
    this.setData({
      isShow: false,
      page_num: 1,
      page_total: 1,
      isRefresh: true
    });
    this.getAnimalList(true, true);
  },
  onReachBottom: function () {
    // 拉到底部加载更多
    if (this.data.isRefresh) {
      //下拉刷新触发该事件时
      this.setData({
        isRefresh: false
      });
      return;
    } else {
      this.setData({
        page_num: this.data.page_num + 1
      })
      this.getAnimalList(false, false);
    }
  },
  onShareAppMessage: function () {
    //页面分享
    return {
      title: '寻找爱宠',
      desc: '它已是家里的一份子，有爱的你看到它请给我联系，家人都在等它。',
      path: '/seek/seek'
    }
  },
  changeQuery: function (e) {
    this.setData({
      query_str: e.detail.value,
      page_num: 1,
      page_total: 1
    });
  },
  search: function (e) {
    this.getAnimalList(true);
  },
  getAnimalList: function (isFirst, isRefresh) {
    var that = this,
      { page_size, page_num, page_total, query_str } = that.data;
    if (!isFirst && (page_num > page_total)) {
      that.setData({
        reminder: "已显示全部内容",
        isShow: true
      });
      return;
    }
    // wx.showToast({
    //   title: '加载中',
    //   icon: 'loading',
    // });
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Info',
      data: {
        'Type': 1,
        'IsOwn': false,
        'Query': query_str,
        'Page': page_num,
        'Size': page_size,
        'City': app.globalData.currCity
      },
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        // wx.hideToast();
        //停止下拉刷新
        if (isRefresh) {
          wx.stopPullDownRefresh();
          that.setData({
            isRefresh: false
          });
        }
        // success
        if (res.statusCode === 200 && res.data.State === 1) {
          var data = res.data.Rows;
          if (data.length === 0) {
            that.setData({
              list: [],
              reminder: "没有符合条件的信息",
              isShow: true
            });
            return;
          } else {
            that.setData({
              isShow: false
            })
          }
          for (let i = 0, len = data.length; i < len; i++) {
            var createTime = data[i].CreateDateTime.substr(0, 10);
            data[i].CreateDateTime = createTime;
          }
          //onload或refresh时计算总页数
          if (isFirst) {
            that.setData({
              list: data,
              page_total: Math.ceil(res.data.Total / page_size)
            })
          } else {
            that.setData({
              list: that.data.list.concat(data)
            });
          }
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '请求失败',
            content: '刷新试试',
            showCancel: false
          })
        }
      },
      fail: function () {
        // fail
        console.log('获取寻找宠物列表失败');
      }
    });
  }
})
