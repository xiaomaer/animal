var app = getApp();
Page({
  data: {
    detail: {}
  },
  onLoad: function (options) {
    this.getSolutionInfo(options.id);
  },
  getSolutionInfo: function (ID) {
    var that = this;
    wx.showToast({
      title: '加载中',
      icon: 'loading',
    });
    //获取问题解决方案详情
    wx.request({
      url: 'https://wxapi.dobit.top/Api/Question/' + ID,
      method: 'GET',
      header: {
        SessionID: wx.getStorageSync('session_id')
      },
      success: function (res) {
        wx.hideToast();
        // success
        if (res.statusCode === 200) {
          that.setData({
            detail: res.data
          });
        } else {
          if (res.statusCode === 401) {
            app.getSessionID();
          }
          wx.showModal({
            title: '获取问题解决方案失败',
            content: '刷新试试',
            showCancel: false
          })
        }
      },
      fail: function () {
        // fail
        console.log('获取宠物详情失败');
      }
    })
  }
});